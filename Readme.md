<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<document type="com.apple.InterfaceBuilder3.CocoaTouch.XIB" version="3.0" toolsVersion="13142" targetRuntime="iOS.CocoaTouch" propertyAccessControl="none" useAutolayout="YES" useTraitCollections="YES" useSafeAreas="YES" colorMatched="YES">
    <dependencies>
        <plugIn identifier="com.apple.InterfaceBuilder.IBCocoaTouchPlugin" version="12042"/>
    </dependencies>
    <objects>
        <placeholder placeholderIdentifier="IBFilesOwner" id="-1" userLabel="File's Owner"/>
        <placeholder placeholderIdentifier="IBFirstResponder" id="-2" customClass="UIResponder"/>
    </objects>
</document>



Assumptions: - 
1. I assume that this application in future, It can be implement other two tabs also, So this application requires Api calls for Json responses and also consider complexity of the project, so we followed the MVVM architecture pattern instead of  MVC architecture pattern for separation and loosely couple of the code.
2. We assume that this application in future required for both iPhone and iPad, so I designed same way for iPhone and iPad(Universal).
3. In this application now I load json response by HOOK , in future we will get response from server so meanwhile I have added folder structure for network call and api's.
4. I have designed and tested on three devices simulators - iPhone SE, iPhone 11, iPad Air.
5. Porfolio details screen we have added screen with navigation title, same for document and setting tab.




  
