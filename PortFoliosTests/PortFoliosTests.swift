//
//  PortFoliosTests.swift
//  PortFoliosTests
//
//  Created by Ganesh Deore on 28/12/21.
//  Copyright © 2021 Ganesh Deore. All rights reserved.
//

import XCTest
@testable import PortFolios

class PortFoliosViewModelTests: XCTestCase {
    
    ///Systen Under Test
    let sut = PortFolioViewModel()
    var portfoliosListViewController = PortfoliosListViewController()
    
    func testCaseGetJSONResponse() {
        let portFolioObj = sut.loadJson(fileName: Constant.FileName.jsonFileName)
        XCTAssertNotNil(portFolioObj)
    }
    
    override func setUp() {
        super.setUp()
        let storyBoard : UIStoryboard = Constant.mainStoryBoard
        portfoliosListViewController = storyBoard.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifier.portfoliosListViewController) as! PortfoliosListViewController
        _  = portfoliosListViewController.view
    }
    
    func testHasTableView() {
        portfoliosListViewController.setUpUIView()
        _  = portfoliosListViewController.view
        XCTAssertNotNil(portfoliosListViewController.portfoliosListTable)
    }
    
    func testLoadTableViewDataSource() {
        XCTAssertTrue(portfoliosListViewController.portfoliosListTable.dataSource != nil)
    }
    
    func testLoadTableViewDelegate() {
        XCTAssertTrue(portfoliosListViewController.portfoliosListTable.delegate != nil)
    }
    
    func testTableViewHasCells() {
        let cell = portfoliosListViewController.portfoliosListTable.dequeueReusableCell(withIdentifier: Constant.TableViewCellIdentifier.portfoliosTableViewCell)
        XCTAssertNotNil(cell,
                        "TableView should be able to dequeue cell with identifier: 'PortfoliosTableViewCell'")
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    
}
