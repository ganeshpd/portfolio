//
//  Extention.swift
//  PortFolios
//
//  Created by Ganesh Deore on 29/12/21.
//  Copyright © 2021 Ganesh Deore. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    /// This function use for keyboard dismis on tap gesture on uiview.
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    ///This function call view editing method.
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
