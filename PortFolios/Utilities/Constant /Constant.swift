//
//  Constant.swift
//  PortFolios
//
//  Created by Ganesh Deore on 29/12/21.
//  Copyright © 2021 Ganesh Deore. All rights reserved.
//

import Foundation
import UIKit

struct Constant {
    
    ///Constant Declarations.
    static let estimatedRowHeight = 44
    static let empty = ""
    static let cornerRadius = 4
    static let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

    ///TableView view cell Identifiers.
    struct TableViewCellIdentifier {
        static let portfoliosTableViewCell = "PortfoliosTableViewCell"
    }
    
    ///ViewController Identifiers.
    struct ViewControllerIdentifier {
        static let portFolioDetailListViewController = "PortFolioDetailListViewController"
        static let portfoliosListViewController = "PortfoliosListViewController"
    }
    
    ///TableView view cell Identifiers.
    struct NavigatioTitleIdentifier {
        static let documents = "Documents"
        static let portfolioDetails = "Portfolio Details"
        static let settings = "Settings"
    }
    
    ///File Name Identifiers.
    struct FileName {
        static let jsonFileName = "PortfolioData"
    }
    
    ///File type Identifiers.
    struct FileType {
        static let Json = "json"
    }
    
    ///Currncy type Identifiers.
    struct CurrncyType {
        static let CHF = "CHF"
        static let GBP = "GBP"
        static let USD = "USD"
    }
    
    ///Response messages
    struct ResponseMessage {
        static let success = "Success"
        static let failure = "something went wrong"
    }
    
}


