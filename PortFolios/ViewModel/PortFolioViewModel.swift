//
//  PortFolioViewModel.swift
//  PortFolios
//
//  Created by Ganesh Deore on 30/12/21.
//  Copyright © 2021 Ganesh Deore. All rights reserved.
//

import Foundation

class PortFolioViewModel {
    
    //MARK:- Variable declaration.
    var portfoliosObjects: [PortfolioElement] = []
    var searchedPortFolioArr: [PortfolioElement] = []
    var didReceivedSuccess: ((String) -> Void)?
    var didReceivedError: ((String) -> Void)?
    
    //MARK:- Custom methods.
    ///This function use for geting json data objects from json file.
    func getPortfolios() {
        if let portFolio = loadJson(fileName: Constant.FileName.jsonFileName) {
            portfoliosObjects = portFolio.portfolios
            if portfoliosObjects.count > 0 {
                didReceivedSuccess!(Constant.ResponseMessage.success)
            } else {
                didReceivedError!(Constant.ResponseMessage.failure)
            }
        }
    }
    
    /// This function use for filter records from array.
    func searchedPortfolioWith(_ searchedText: String) {
        searchedPortFolioArr = portfoliosObjects.filter { $0.name.lowercased().contains(searchedText.lowercased()) }
    }
    
    //MARK:- Custom Methods.
    /// This function use for reading json data and parse with codable.
    func loadJson(fileName: String) -> PortfolioDataResponse? {
        let decoder = JSONDecoder()
        guard
            let url = Bundle.main.url(forResource: fileName, withExtension: Constant.FileType.Json),
            let data = try? Data(contentsOf: url),
            let portfolioDataResponse = try? decoder.decode(PortfolioDataResponse.self, from: data)
            else {
                return nil
        }
        return portfolioDataResponse
    }
    
}
