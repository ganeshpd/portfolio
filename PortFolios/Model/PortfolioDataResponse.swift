//
//  PortfolioDataResponse.swift
//  PortFolios
//
//  Created by Ganesh Deore on 28/12/21.
//  Copyright © 2021 Ganesh Deore. All rights reserved.
//

import Foundation
import UIKit

//MARK: - Portfolio Response parsing.
struct PortfolioDataResponse: Codable {
    let portfolios: [PortfolioElement]
}

///Enum for assigned color as per currancy.
enum CurrancyName: String, Codable {
    case CHF = "CHF"
    case GBP = "GBP"
    case USD = "USD"
    
    func color() -> UIColor {
        switch self {
        case .CHF:
            return #colorLiteral(red: 0.662745098, green: 0.5411764706, blue: 0.462745098, alpha: 1)
        case .GBP:
            return #colorLiteral(red: 0, green: 0.4549019608, blue: 0.8039215686, alpha: 1)
        case .USD:
            return  #colorLiteral(red: 0.1725490196, green: 0.2509803922, blue: 0.3529411765, alpha: 1)
        }
    }
}

// MARK: - PortfolioElement
struct PortfolioElement: Codable {
    var id, name, value: String
    let currency: CurrancyName
}

