//
//  PortfoliosTableViewCell.swift
//  PortFolios
//
//  Created by Ganesh Deore on 28/12/21.
//  Copyright © 2021 Ganesh Deore. All rights reserved.
//

import UIKit

class PortfoliosTableViewCell: UITableViewCell {
    
    //Mark: IBoutlets
    @IBOutlet weak var portfolioTypeLbl: UILabel!
    @IBOutlet weak var portfolioIDLbl: UILabel!
    @IBOutlet weak var portfolioValueLbl: UILabel!
    @IBOutlet weak var portfolioCurrencyLbl: UILabel!
    
    //MARK:- View life Cycle methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Custom methods.
    /// This function use for Configure cell with portfolio datasource objects.
    func configureCell(_ portfolioElement :PortfolioElement,_ indexPath: IndexPath) {
        self.portfolioTypeLbl.text = portfolioElement.name
        self.portfolioIDLbl.text = portfolioElement.id
        self.portfolioValueLbl.text = portfolioElement.value
        self.portfolioCurrencyLbl.text = portfolioElement.currency.rawValue
        self.portfolioCurrencyLbl.backgroundColor = portfolioElement.currency.color()

        self.accessoryType = .disclosureIndicator
        self.selectionStyle = .none
        self.portfolioCurrencyLbl.layer.cornerRadius = CGFloat(Constant.cornerRadius)
        self.portfolioCurrencyLbl.layer.masksToBounds = true
    }
}
