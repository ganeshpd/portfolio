//
//  ViewController.swift
//  PortFolios
//
//  Created by Ganesh Deore on 28/12/21.
//  Copyright © 2021 Ganesh Deore. All rights reserved.
//

import UIKit

class PortfoliosListViewController: UIViewController {
    
    //Mark:- IBOutlets
    @IBOutlet weak var noRecordsFoundLbl: UILabel!
    @IBOutlet weak var portfolioSearchBar: UISearchBar!
    @IBOutlet weak var portfoliosListTable: UITableView!
    
    //MARK:- Variable Declarations
    var isSearching: Bool = false
    var portFolioViewModel = PortFolioViewModel()
    
    //MARK:- View life cycle methods.
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    /// This dinit method use to release objects where we allocatted in memory.
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- Custom methods
    ///This function use for to setup intial UI of view.
    func setUpUIView() {
        bindViewModel()
        portfolioSearchBar.delegate = self
        portfolioSearchBar.showsCancelButton = true
        self.portfoliosListTable.register(UINib(nibName: Constant.TableViewCellIdentifier.portfoliosTableViewCell,
                                                bundle: nil),
                                          forCellReuseIdentifier: Constant.TableViewCellIdentifier.portfoliosTableViewCell)
        portfoliosListTable.delegate = self
        portfoliosListTable.dataSource = self
        portfoliosListTable.rowHeight = UITableView.automaticDimension
        portfoliosListTable.estimatedRowHeight = CGFloat(Constant.estimatedRowHeight)
        portFolioViewModel.getPortfolios()
        portfoliosListTable.tableFooterView = UIView()
        noRecordsFoundLbl.isHidden = true
        portfoliosListTable.keyboardDismissMode = .onDrag
        addObserversForCheckViewStatus()
    }
    
    /// This function use for to register observers for to check view status as background or forground
    func addObserversForCheckViewStatus() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground),
                                       name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appCameToForeground),
                                       name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    /// This function call when view on background mode.
    @objc func appMovedToBackground() {
        view.isHidden = true
    }

    /// This function call when view on foreground mode.
    @objc func appCameToForeground() {
        view.isHidden = false
    }
    
    /// This function use for bind viewmodel to ViewController
    func bindViewModel() {
        portFolioViewModel.didReceivedSuccess = { responseMsg in
            self.portfoliosListTable.reloadData()
        }
        portFolioViewModel.didReceivedError = { errorMsg in
            self.noRecordsFoundLbl.text = Constant.ResponseMessage.failure
        }
    }
    
    ///This method use to move to detail screen of list.
    func moveToDetailView() {
        let storyBoard : UIStoryboard = Constant.mainStoryBoard
        let portFolioDetailListViewController = storyBoard.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifier.portFolioDetailListViewController) as! PortFolioDetailListViewController
        self.navigationController?.pushViewController(portFolioDetailListViewController, animated: true)
    }
    
}

//MARK:- UITableViewDataSource Methods.
extension PortfoliosListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching ? portFolioViewModel.searchedPortFolioArr.count : portFolioViewModel.portfoliosObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PortfoliosTableViewCell = tableView.dequeueReusableCell(withIdentifier: Constant.TableViewCellIdentifier.portfoliosTableViewCell, for: indexPath) as! PortfoliosTableViewCell
        let portfolioElement = isSearching ? portFolioViewModel.searchedPortFolioArr[indexPath.row] : portFolioViewModel.portfoliosObjects[indexPath.row]
        cell.configureCell(portfolioElement, indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- UITableViewDelegate Methods.
extension PortfoliosListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //TODO:- Navigate to Detail page.
        moveToDetailView()
    }
}

//MARK:- UISearchBarDelegate Methods.
extension PortfoliosListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        portFolioViewModel.searchedPortfolioWith(searchText)
        isSearching = searchText.isEmpty ? false : true
        if portFolioViewModel.searchedPortFolioArr.count == 0 {
            portfoliosListTable.isHidden = isSearching
            noRecordsFoundLbl.isHidden = !isSearching
        } else {
            portfoliosListTable.isHidden = false
            noRecordsFoundLbl.isHidden = true
        }
        DispatchQueue.main.async {
            self.portfoliosListTable.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        portfolioSearchBar.text = Constant.empty
        isSearching = false
        portfoliosListTable.isHidden = false
        noRecordsFoundLbl.isHidden = true
        portfoliosListTable.reloadData()
        self.portfolioSearchBar.endEditing(true)
    }
}
