//
//  DocumentsViewController.swift
//  PortFolios
//
//  Created by Ganesh Deore on 02/01/22.
//  Copyright © 2022 Ganesh Deore. All rights reserved.
//

import UIKit

class DocumentsViewController: UIViewController {

    //MARK:- View life cycle methods.
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUIView()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Custom methods
    ///This function use for to setup intial UI of view.
    func setUpUIView() {
        self.navigationItem.title = Constant.NavigatioTitleIdentifier.documents
    }
}
