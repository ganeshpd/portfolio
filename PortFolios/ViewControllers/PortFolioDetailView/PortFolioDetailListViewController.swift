//
//  PortFolioDetailListViewController.swift
//  PortFolios
//
//  Created by Ganesh Deore on 01/01/22.
//  Copyright © 2022 Ganesh Deore. All rights reserved.
//

import UIKit

class PortFolioDetailListViewController: UIViewController {
    
    //MARK:- View life cycle methods.
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUIView()
    }
    
    //MARK:- Custom methods
    ///This function use for to setup intial UI of view.
    func setUpUIView() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = Constant.NavigatioTitleIdentifier.portfolioDetails
    }
    
}
